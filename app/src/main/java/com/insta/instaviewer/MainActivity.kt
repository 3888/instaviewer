package com.insta.instaviewer

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.insta.instaviewer.databinding.ActivityMainBinding
import timber.log.Timber

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setupNavigation()
    }

    private fun setupNavigation() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_outline_photo_camera_24)

        binding.menuBottom.setOnNavigationItemSelectedListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> Toast.makeText(this, "TODO open camera", Toast.LENGTH_SHORT).show()
            R.id.sendMessageFragment -> NavigationUI.onNavDestinationSelected(
                item, findNavController(R.id.myNavHostFragment)
            )
        }

        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        when (id) {
            //  TODO add item handling
            R.id.menu_home -> {
            }
            R.id.menu_search -> {
            }
            R.id.menu_add -> {
            }
            R.id.menu_favorite -> {
            }
            R.id.menu_user -> {
            }
        }

        Toast.makeText(this, "Action id is $id", Toast.LENGTH_SHORT).show()
        return true
    }
}