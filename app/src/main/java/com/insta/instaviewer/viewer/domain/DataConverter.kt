package com.insta.instaviewer.viewer.domain

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.IOException

object DataConverter {
    private const val FAKE_DATA = "Data.json"

    fun getData(context: Context): List<ViewerData> {
        val builder = GsonBuilder()
        val gson = builder.create()

        val listDataType = object : TypeToken<List<ViewerData>>() {}.type

        return gson.fromJson(
            getFakeData(context),
            listDataType
        )
    }

    private fun getFakeData(context: Context): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(FAKE_DATA).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}