package com.insta.instaviewer.viewer.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.insta.instaviewer.databinding.ItemNewViewerBinding
import com.insta.instaviewer.viewer.domain.ViewerData

class ViewerAdapter(val clickListener: ClickListener) :
    RecyclerView.Adapter<ViewerAdapter.ViewHolder>() {

    var data = listOf<ViewerData>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(clickListener, item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ItemNewViewerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: ClickListener, item: ViewerData) {
            binding.newData = item
            binding.clickListener = listener

            // TODO add logic of count  of users shown
            if (getLikedByList(item).length > MAX_LENGTH_OF_LIKED_BY_LIST) {
                val text = (getLikedByList(item)
                    .take(MAX_LENGTH_OF_LIKED_BY_LIST)
                    .plus("... and ${item.list_liked_by.size - 2} others"))

                binding.tvLikedByList.text = text

            } else {
                binding.tvLikedByList.text = getLikedByList(item)
            }

            // This is important, because it forces the data binding to execute immediately,
            // which allows the RecyclerView to make the correct view size measurements
            binding.executePendingBindings()


        }

        private fun getLikedByList(card: ViewerData): String {
            val stringBuilder = StringBuilder()
            val sizeLikedList = card.list_liked_by.size
            card.list_liked_by.forEachIndexed { index, user ->
                stringBuilder.append(user.nick)
                if (index != sizeLikedList - 1)
                    stringBuilder.append(", ")
            }
            return stringBuilder.toString()
        }


        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemNewViewerBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }

            const val MAX_LENGTH_OF_LIKED_BY_LIST = 30

        }
    }
}

class ClickListener(val clickListener: (int: View) -> Unit) {
    fun onClick(view: View) = clickListener(view)
}

