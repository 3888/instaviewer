
package com.insta.instaviewer.viewer.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.android.trackmysleepquality.sleeptracker.ViewerModelFactory
import com.insta.instaviewer.R
import com.insta.instaviewer.databinding.FragmentNewViewerBinding
import com.insta.instaviewer.viewer.ViewerViewModel
import com.insta.instaviewer.viewer.domain.DataConverter

class ViewerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentNewViewerBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_new_viewer, container, false
        )

        val application = requireNotNull(this.activity).application

        val dataSource = DataConverter

        val viewModelFactory = ViewerModelFactory(dataSource, application)

        val viewModel =
            ViewModelProviders.of(
                this, viewModelFactory
            ).get(ViewerViewModel::class.java)

        binding.newViewModel = viewModel
        binding.lifecycleOwner = this

        val adapter = ViewerAdapter(ClickListener {
            Toast.makeText(requireContext(), "Action id is ${it.id}", Toast.LENGTH_SHORT).show()
        })
        binding.viewerList.adapter = adapter

        return binding.root
    }
}
