package com.insta.instaviewer.viewer

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.insta.instaviewer.viewer.domain.DataConverter
import com.insta.instaviewer.viewer.domain.ViewerData
import kotlinx.coroutines.*

/**
 * ViewModel for ViewerFragment.
 */
class ViewerViewModel(
        private val viewerConverter: DataConverter,
        application: Application
) : AndroidViewModel(application) {

        private var viewModelJob = Job()

        private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

        private val _viewerData = MutableLiveData<List<ViewerData>>()

        val viewerData: LiveData<List<ViewerData>>
                get() = _viewerData

        override fun onCleared() {
                super.onCleared()
                viewModelJob.cancel()
        }

        init {
                initializeData()
        }

        private fun initializeData() {
                uiScope.launch {
                        _viewerData.value = getDataFromFakeDatabase()
                }
        }

        private suspend fun getDataFromFakeDatabase(): List<ViewerData> {
                return withContext(Dispatchers.IO) {
                        DataConverter.getData(getApplication())
                }
        }
}

