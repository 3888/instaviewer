
package com.insta.instaviewer.viewer.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.insta.instaviewer.R
import com.insta.instaviewer.databinding.FragmentSendMessageBindingImpl

class SendMessageFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentSendMessageBindingImpl = DataBindingUtil.inflate(
                inflater,
            R.layout.fragment_send_message, container, false)

        return binding.root
    }
}
