package com.insta.instaviewer.viewer.domain

import com.google.gson.annotations.SerializedName


data class ViewerData(
    val nickname: String,
    val city: String,
    @SerializedName("avatar_url")
    val avatarImageUrl: String,
    val photo: String,
    @SerializedName("liked_by")
    val list_liked_by: List<User>,
    val description: String
)

data class User(
    val id: String,
    val nick: String
)